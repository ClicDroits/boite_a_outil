import os
from StringIO import StringIO
import csv
import re
import sys
import requests
import json

def clear_comment(s1, deli):
    tab = s1.split('\n')
    ftab = ""
    isp = tab[0].strip() != deli
    for i in tab:
        if i.strip() == deli:
            isp ^= 1
        elif isp == 1:
            ftab += i + '\n'
    return ftab;

def removeComments(string):
    string += '\n'
    string = clear_comment(string, "'''")
    string = clear_comment(string , '"""')
    string = re.sub(re.compile("/\*.*?\*/",re.DOTALL ) ,"" ,string) 
    string = re.sub(re.compile("'''.*?'''",re.DOTALL ) ,"" ,string) 
    string = re.sub(re.compile("#.*?\n" ) ,"" ,string) 
    return string

def check_is_variable(name, parent):
    url = "https://fr.openfisca.org/api/v20/variable/" + name
    requete = requests.get(url.encode(encoding="UTF-8"))
    if requete.status_code != 200:
        error_list.append(url + " from : " + "https://legislation.openfisca.fr/" + parent);
        return 0
    page = requete.content
    json_data = json.loads(page, encoding=requete.encoding);
    if json_data == None or json_data == {} or 'error' in json_data:
        return 0
    return 1


already_visit = []
error_list = []

class DataReader(object):

    """Docstring for DataReader. """


    def read_formula(self, formule):
        string = removeComments(formule)
        tab = string.split('\'')
        dependence = []
        for i in range(0, len(tab) - 1):
            if i % 2 == 1 and tab[i - 1].find('bareme_name') == -1 and check_is_variable(tab[i], self.parent) == 1:
                print tab[i]
                dependence.append(tab[i])
        return dependence

    def get_json_data(self, name, parent):
        url = "https://fr.openfisca.org/api/v20/variable/" + name
        for i in already_visit:
            if i == name:
                return {"error" : "Already visit"}
        already_visit.append(name);
        requete = requests.get(url.encode(encoding="UTF-8"))
        if requete.status_code != 200:
            error_list.append(url + " from : " + "https://legislation.openfisca.fr/" + parent);
            return {"error" : "server Connection "}
        page = requete.content
        json_data = json.loads(page, encoding=requete.encoding);
        return json_data
            

    def find_last_formula(self, json_data):
        if len(json_data['formulas']) == 0 or json_data['formulas'] is None:
            self.is_variable = 1
        elif json_data['formulas'] is not 'null':
            if len(json_data['formulas']) >= 1:
                self.formule = json_data['formulas'][json_data['formulas'].keys()[-1:][0]]
                if self.formule is not None:
                    self.formule = self.formule['content']
            else:
                self.formule = json_data['formulas']['0001-01-01']['content']
        else:
            error_list.append("Error for {:s} formulas is null".format(name));
        return self.formule

    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.dependence = []
        self.formules = []
        self.formule = ""
        self.is_variable = 0

        json_data = self.get_json_data(name, parent)
        if 'error' in json_data:
            return
        if self.find_last_formula(json_data) is not None:
            self.dependence = self.read_formula(self.formule)

    def recur_build_json(self):
        if  len(self.dependence) == 0:
            return {
                    "name": self.name,
                    "parent": self.parent}
        children = []
        for dep in self.dependence:
            tmp = DataReader(dep, self.name)
            children.append(tmp.recur_build_json())
        return {
                    "name": self.name,
                    "parent": self.parent,
                    "children": children}

    def __str__(self):
        str_return = "Formule :\n" + str(self.formule) + "\nDependence :\n"
        str_return += "{\n"
        str_return += str(self.dependence)
        str_return += "\n}\n"
        return str_return


if __name__ == "__main__":
    print check_is_variable('timedelta64[Y]', 'd')
    if len(sys.argv) == 3:
        aide = sys.argv[1]
        filename = sys.argv[2]
    else:
        print "Usage : python get_variable_aide.py [aide] [file.json]"
        print "Continue with aide=apl"
        filename = 'file.json'
        aide = 'apl'

    io = StringIO()
    data = DataReader(aide, "ROOT")
    datajson = data.recur_build_json()
    json.dump(datajson, io)
    file = open(filename, "w+")
    file.write(io.getvalue())
    file.close()
