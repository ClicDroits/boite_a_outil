from openfisca_france import CountryTaxBenefitSystem
from openfisca_core.simulations import Simulation
from situation_examples import celibataire
from situation_examples import celibataire2
import time
import sys

tax_benefit_system = CountryTaxBenefitSystem()

aide = 'rsa'

simulation = Simulation(tax_benefit_system = tax_benefit_system, simulation_json = celibataire)
print ('Simulation celibataire 1 status tns (non calculable) = ' + str (simulation.calculate(aide, '2016-02')))
simulation = Simulation(tax_benefit_system = tax_benefit_system, simulation_json = celibataire2)
print 'Simulation celibataire 2 status calculable = ' + str(simulation.calculate(aide, '2016-02'))
