import os
from StringIO import StringIO
import csv
from Database import *
import re
import sys
import requests
import json

def clear_comment(s1, deli):
    tab = s1.split('\n')
    ftab = ""
    isp = tab[0].strip() != deli
    for i in tab:
        if i.strip() == deli:
            isp ^= 1
        elif isp == 1:
            ftab += i + '\n'
    return ftab;

def removeComments(string):
    string += '\n'
    string = clear_comment(string, "'''")
    string = clear_comment(string , '"""')
    string = re.sub(re.compile("/\*.*?\*/",re.DOTALL ) ,"" ,string) 
    string = re.sub(re.compile("'''.*?'''",re.DOTALL ) ,"" ,string) 
    string = re.sub(re.compile("#.*?\n" ) ,"" ,string) 
    return string

def check_is_variable(name, parent):
    url = "https://fr.openfisca.org/api/v20/variable/" + name
    requete = requests.get(url.encode(encoding="UTF-8"))
    if requete.status_code != 200:
        error_list.append(url + " from : " + "https://legislation.openfisca.fr/" + parent);
        return 0
    page = requete.content
    json_data = json.loads(page, encoding=requete.encoding);
    if json_data == None or json_data == {} or 'error' in json_data:
        print 'error check is variable' + str(json_data)
        return 0
    return 1

def get_json(url):
    requete = requests.get(url.encode(encoding="UTF-8"))
    if requete.status_code != 200:
        error_list.append(url + " from : " + "https://legislation.openfisca.fr/" + parent);
        return {"error" : "server Connection "}
    page = requete.content
    json_data = json.loads(page, encoding=requete.encoding);
    return json_data

def get_variable_array():
    json = get_json("https://fr.openfisca.org/api/v20/variables")
    tab = []
    for i in json:
        tab.append(i)
    return tab

already_visit = []
error_list = []
variables = get_variable_array()

class DataReader(object):

    """Docstring for DataReader. """


    def read_formula(self, formule):
        dependence = []
        for v in variables:
            pos = self.formule.find("\'" + v + "\'")
            if  pos != -1:
                dependence.append(v)
        return dependence

    def get_json_data(self, name, parent):
        url = "https://fr.openfisca.org/api/v20/variable/" + name
        return get_json(url)
            

    def find_last_formula(self, json_data):
        if len(json_data['formulas']) == 0 or json_data['formulas'] is None:
            self.is_variable = 1
        elif json_data['formulas'] is not 'null':
            if len(json_data['formulas']) >= 1:
                self.formule =  json_data['formulas'][json_data['formulas'].keys()[0]]
                if self.formule is not None:
                    self.formule = self.formule['content']
                    return self.formule
            else:
                self.formule = json_data['formulas']['0001-01-01']['content']
        else:
            error_list.append("Error for {:s} formulas is null".format(name));
        return self.formule

    def get_json_var(self, var, json_data):
        if var in json_data:
            return json_data[var]
        else:
            return ""

    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.dependence = []
        self.formules = []
        self.formule = ""
        self.is_variable = 0
        self.db = Database()

        json_data = self.get_json_data(name, parent)
        if 'error' in json_data:
            return
        self.entity = self.get_json_var('entity', json_data)
        self.desc = self.get_json_var('description', json_data)
        self.valType = self.get_json_var('valueType', json_data)
        self.formule = self.find_last_formula(json_data)
        if self.formule is not None:
            self.dependence = self.read_formula(self.formule)
        else:
            self.formule = ""
    #    self.json = self.build_json()

    def recur_build_json(self):
        if  len(self.dependence) == 0:
            return {
                    "name": self.name,
                    "parent": self.parent}
        children = []
        for dep in self.dependence:
            tmp = DataReader(dep, self.name)
            children.append(tmp.recur_build_json())
        return {
                    "name": self.name,
                    "parent": self.parent,
                    "children": children}
    def insert_db(self):
        if hasattr(self, 'desc'):
            self.db.insert("prestations_variables_template_child", {
                "name" : self.name, 
                "description" : self.desc,
                "valueType" : self.valType,
                "entity" : self.entity,
                "formula" : removeComments(self.formule),
                "children" :':'.join(self.dependence),
                "json" : json.dumps(self.json)
                })
            self.db.commit()

    def build_json(self):
        json = dict();
        json["name"] = self.name;
        json["parent"] = self.parent;
        json["entity"] = self.entity;
        json["type"] = self.valType;
        json["nb_children"] = len(self.dependence);
        json["children"] = [];
        for dep in self.dependence:
            child_rd = DataReader(dep, 'dtCare')
            json["children"].append({
                    "name": dep,
                    "parent": self.name, 
                    "nb_children" : len(child_rd.dependence),
                    "type" : child_rd.valType,
                    "entity" : child_rd.entity,
                    });
        return json


    def __str__(self):
        str_return = "Formule :\n" + str(self.formule) + "\nDependence :\n"
        str_return += "{\n"
        str_return += str(self.dependence)
        str_return += "\n}\n"
        str_return += "json :" + str(self.json)
        return str_return


if __name__ == "__main__":
    if len(sys.argv) == 3:
        aide = sys.argv[1]
        filename = sys.argv[2]
    else:
        print "Usage : python Data [aide]"
        print "Continue with aide=apl"
        filename = 'file.json'
        aide = 'apl'

    io = StringIO()
    data = DataReader(aide, "ROOT")
    datajson = data.recur_build_json()
    json.dump(datajson, io)
    file = open("file.json", "w+")
    file.write(io.getvalue())
    file.close()
