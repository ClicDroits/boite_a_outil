from DataReader import *
from Database import *
import argparse
import sys
import json

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    io = StringIO()
    parser.add_argument("--json",  action="store_true", help="Affiche les dependence en json sur la sortit standart pour l\'aide donner")
    parser.add_argument("--build",  action="store_true", help="Remplit la base de donner des dependences de l\'aide")
    parser.add_argument('-o', "--output" , help="Precise le fichier de destination du json")
    parser.add_argument('-a', '--aide', help ="Selectionner votre aide : [apl, rsa, aah, paje, ...]")
    args = parser.parse_args()


    if not args.build and not args.json:
        parser.print_help()
        sys.exit(1)

    if args.aide is not None:
        aide = args.aide
    else:
        print 'Continue avec aide = apl'
        aide = 'apl'
    i = 0;
    if args.build:
        for v in variables:
            data = DataReader(v, "ROOT")
            data.json = data.build_json()
            data.insert_db()
            i += 1;
            print (float(i) * 100.0) / len(variables);
        print (len(variables))
    if args.json and args.output:
        file = open(args.output, "w+")
        json_dp =  data.recur_build_json()
        json.dump(json_dp, io)
        file.write(io.getvalue())
        file.close()
    elif args.json:
        print data.recur_build_json()
