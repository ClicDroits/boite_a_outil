/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : clicdroits

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-04-03 18:50:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for prestations_variables_template
-- ----------------------------
DROP TABLE IF EXISTS `prestations_variables_template_child`;
CREATE TABLE `prestations_variables_template_child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `children` varchar(2048) DEFAULT NULL,
  `description` varchar(2048) DEFAULT NULL,
  `valueType` varchar(255) DEFAULT NULL,
  `entity` varchar(255) DEFAULT NULL,
  `formula` varchar(8192) DEFAULT NULL,
  `json` varchar(8192) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=393 DEFAULT CHARSET=latin1;
