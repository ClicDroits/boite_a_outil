/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : clicdroits

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-04-03 18:50:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for prestations_variables_template
-- ----------------------------
DROP TABLE IF EXISTS `prestations_variables_template`;
CREATE TABLE `prestations_variables_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prestation_name` varchar(255) DEFAULT NULL,
  `parent_name` varchar(255) DEFAULT NULL,
  `children_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `constant` int(1) DEFAULT NULL,
  `valueType` varchar(255) DEFAULT NULL,
  `entity` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=393 DEFAULT CHARSET=latin1;
