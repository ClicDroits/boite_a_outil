var get_min_json = function(name) {
	var js = {
		"individus" : {},
		"menages" : {"_" : {"personne_de_reference" : []}},
		"foyers_fiscaux" : {"_" : {"declarants" : []}},
		"familles" : {"_" : {"parents" : []}},
	}

	// ----- Minimum d'info pour chaque groupe entity ------ //
	js['individus'][name] = {"date_naissance" : {"ETERNITY" : "1980-01-01"}};
	js['menages']["_"]["personne_de_reference"].push(name);
	js['foyers_fiscaux']["_"]["declarants"].push(name);
	js['familles']["_"]["parents"].push(name);
	return js;
}

var build_json = function (name, callback) { 
	var data = get_min_json(name);
	var current_mouth = new Date().toISOString().slice(0,7);
	var nbreq = d3.selectAll('g.node').filter(function(d){return d.__data__['input_value'] && d.__data__['input_value'] != ''})[0].length;
	d3.selectAll('g.node').filter(function(d){return d.__data__['input_value'] && d.__data__['input_value'] != ''}).each(function(d) {
		$.ajax({
			url : "/get_entity?aide=" + d.name,
			success : function (resp) {
				if (resp == 'famille') {
					if (!data['familles']['_'][d.name]) {
						data['familles']['_'][d.name] = {};
					}
					data['familles']['_'][d.name][current_mouth] = d.__data__['input_value'];
				}
				else if (resp == 'foyer_fiscal') {
					if(!data['foyers_fiscaux']['_'][d.name]) {
						data['foyers_fiscaux']['_'][d.name] = {};
					}
					data['foyers_fiscaux']['_'][d.name][current_mouth] = d.__data__['input_value'];
				}
				else if (resp == 'menage') {
					if (!data['menages']['_'][d.name]) {
						data['menages']['_'][d.name] = {};
					}
					data['menages']['_'][d.name][current_mouth] = d.__data__['input_value'];
				}
				else if (resp == 'individu') {
					if (!data['individus'][name][d.name]) {
						data['individus'][name][d.name] = {};
					}
					data['individus'][name][d.name][current_mouth] = d.__data__['input_value'];
				}
				nbreq -= 1;
				if (nbreq == 0) {
					callback(data);
				}
			} 
		});
	}); 
}
