var init_hljs_view = function() {
	hljs.initHighlightingOnLoad();
	$('pre code').each(function(i, block) {
		hljs.highlightBlock(block);
	});
}

var selected = undefined;
var current_node;
var interval;

var get_simulation = function(data) {
	$.post("/run_simulation?aide=" + $('#aide').html(), {js : JSON.stringify(data)}).done(function(resp){
		alert("Vous aver le droit a " + resp + "e pour l'aide :" + $('#aide').html());
		d3.selectAll('g.node').filter(function(d){return d.__data__['input_value'] &&
				d.__data__['input_value'] != ""}).each(function(d){
					d.__data__['input_value'] = "";
				});
	})	
}


var click = function(d) {
	current_node = d;
	if (d.children == null && d._children == null) {
		if (exist_in_json(d.name, get_root(d)) == 0) {
			add_in_json(d.name, selected.root, d.parent.name);
		}
		else {
		}
	}
	if (d.children) {
		d._children = d.children;
		d.children = null;
	} else if (!d.__data__['input_value']) { 
		d.children = d._children;
		d._children = null;
	}
	update(d, selected)
	update_json_info(d.name);
}


var update = function(source, viewer) {
	var nodes = viewer.tree.nodes(viewer.root).reverse();
	var links = viewer.tree.links(nodes);
	nodes.forEach(function(d) { d.y = d.depth * 350; });


	// --------------  Node ----------------- //
	var node = viewer.svg.selectAll("g.node").data(nodes, function(d) { return d.id || (d.id = ++viewer.it); });

	// --------------  Node enter ----------------- //
	var nodeEnter = node.enter().append("g")
		.attr("class", "node")
		.attr("transform", function(d) { 
			d.__data__ = {"nb_child" : d.nb_child};
			return "translate(" + source.y0 + "," + source.x0 + ")"; })
		.on("click", click)

	nodeEnter.append("circle")
		.attr("r", 6)
		.style("fill", "lightsteelblue");

	nodeEnter.append("text")
		.attr("x", function(d) { return d.children || d._children ? -10 : 10; })
		.attr("dy", ".35em")
		.attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
		.text(function(d) { return d.name; })
		.style("fill-opacity", 1e-6);

	// --------------  Node update ----------------- //

	var nodeUpdate = node.transition()
		.duration(viewer.duration)
		.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

	nodeUpdate.select("circle")
		.attr("r", 10)
		.style('fill', function(d){
			var color = "#000";
			if (d.__data__['input_value']) { 
				return "LightGreen";
			}
			else if (node_can_be_calculated(d)) {
				if (d.children){
					d._children = d.children;
					d.children = null;
				}
				return "LightGreen";
			}
			else { 
				color = "lightsteelblue";
			}
			if (d.nb_children == 0) {
				color = "Orange";
			}
			return color;
		}).
		style('stroke-width', '0px');


	nodeUpdate.select("text")
		.style("fill-opacity", 1);

	// --------------  Node exit ----------------- //

	var nodeExit = node.exit().transition()
		.duration(viewer.duration)
		.attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
		.remove();

	nodeExit.select("circle")
		.attr("r", 1e-6);

	nodeExit.select("text")
		.style("fill-opacity", 1e-6);

	// --------------  Link  ----------------- //

	var link = viewer.svg.selectAll("path.link")
		.data(links, function(d) { return d.target.id; });


	// --------------  Link  enter ----------------- //

	var diagonal = d3.svg.diagonal().projection(function(d) {		return [d.y, d.x]; });
	link.enter().insert("path", "g")
		.attr("class", "link")
		.attr("d", function(d) {
			var o = {x: source.x0, y: source.y0};
			return diagonal({target: o, source: o});
		});

	// --------------  Link  update ----------------- //

	link.transition()
		.duration(viewer.duration)
		.attr("d", diagonal);

	// --------------  Link  exit ----------------- //

	link.exit().transition()
		.duration(viewer.duration)
		.attr("d", function(d) {
			var o = {x: source.x, y: source.y};
			return diagonal({source: o, target: o});
		})
		.remove();

	nodes.forEach(function(d) {
		d.x0 = d.x;
		d.y0 = d.y;

	});
	d3.select(self.frameElement).style("height", "800px");
}

function get_db_info_json(aide, ) {
	var info  = $.ajax({
		type: "GET",
		url: "/get_json?aide=" + aide,
		async: false
	}).responseText;
	return info;
}


function SvgViewer (dom_elem, variable, height){
	this.margin = {top: 20, right: 120, bottom: 20, left: 120};
	this.width = 6000 - this.margin.right - this.margin.left;
	this.height = height;
	this.variable = variable;
	this.json = JSON.parse(get_db_info_json(variable));
	this.tree = d3.layout.tree().size([this.height, this.width]);
	this.diagonal = d3.svg.diagonal().projection(function(d) { return [d.y, d.x]; });
	this.svg = d3.select(dom_elem).append("svg")
		.attr("width", this.width + this.margin.right + this.margin.left)
		.attr("height", this.height + this.margin.top + this.margin.bottom).append("g")
		.attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");
	this.it = 0;
	this.duration = 700;
	this.root = this.json;
	this.root.x0 = height / 2;
	this.root.y0 = 0;
	this.nodes = this.tree.nodes(this.root).reverse();
	collapse(this.root);

	this.refresh_graph = function() {
		this.root = this.json;
		this.root.x0 = height / 2;
		this.root.y0 = 0;
		update(this.root, this);
	} 
	this.refresh_graph();
}

var valid_callback = function(v) {
	v = v.data;
	var nodes = v.tree.nodes(v.root).reverse();
	nodes.forEach(function(d) { d.y = d.depth * 250; }); // default 180
	var fdp = 'coucou';

	d3.selectAll('g.node').filter(function(c) { return current_node.name == c.name }).each(function(d){
		if ($('#selected_id').html() == "") {
			alert('Empty selection');
			return;
		}	
		d.__data__["input_value"]  = $('#selected_sql input').val();
		var node = v.svg.selectAll("g.node").data(nodes, function(d) { return d.id || (d.id = ++v.it); }).attr("close", function(d) {
			if (d.name == $('#selected_id').html()) {
				d._children = d.children;
				d.children = null;
			}
		});
	})
	update(v.root, v);
}

$(function() {
	selected =  new SvgViewer("#svg_loc", $('#aide').html(), 500);
	init_hljs_view();
	console.log(selected);
	$('#btn_val').click(selected, valid_callback);
	$('#mytable').dataTable({
	});
	interval = setInterval(function(){ update(selected.root, selected) }, 700);

});
