var period_detect = ["period.this_month", "period.last_month", "period.this_year", "period.last_year", "period.n_2", "period.last_3_months"]

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find), replace);
}

var update_screen_info = function(json_data) {
	var nb_detect = 0;
	var detect = ".";
	$('#selected_id').html(json_data['id']);
	$('#selected_description').html(json_data['description']);
	$('#selected_period').html(json_data['definitionPeriod']);
	$('#selected_entity').html(json_data['entity']);
	$('#selected_valueType').html(json_data['valueType']);
	if (Object.keys(json_data['formulas']).length == 0) {
		$('#selected_formula').html('');
		return ;
	}
		var key = Object.keys(json_data['formulas'])[Object.keys(json_data['formulas']).length - 1]
	if (json_data['formulas'][key] != null && json_data['formulas'][key].length != 0) {
		var formule = json_data['formulas'][key]['content'].replace('\n', '<br/>');
		var cp = formule;
		for (var i = 0, len = period_detect.length; i < len; i++) {
			if (formule.indexOf(period_detect[i]) != -1) {
				formule = replaceAll(formule, period_detect[i], "<span class='hljs-period'>" + period_detect[i] + "</span>");
				nb_detect += 1;
				detect = period_detect[i];
			}
		}
	}
	else {
		var formule = "";
	}
	$('#selected_formula').html(formule);
	if (nb_detect > 1) {
		detect = "???"
	}
	$('#selected_child_period').html("<span class='hljs-period'>" + detect + "</span>");
  hljs.highlightBlock($('pre code')[0]);
}

var update_json_info = function(name) {
	$.ajax({
		url: "https://fr.openfisca.org/api/v20/variable/" + name,
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(thrownError);
		},
		success: function(resp) {
			update_screen_info(resp);
		}
	})
}
