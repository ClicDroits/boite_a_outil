var collapse = function(d) {
	if (d._children) {
		d.children = d._children;
		d.children.forEach(collapse);
		d._children = null;
	}
}

var swap_children = function(d) {
	if (d.children) {
		d._children = d.children;
		d.children = null;
	}
	else {
		d.children = d._children;
		d._children = null;
	}
}

var node_is_set = function(node) {
	if(node__data__["input_value"]) {
		return 1;
	}
	return 0;
}

var node_nb_child = function(d) {
	return $('#nb_child_' + d.name).html();
}

var exist_in_json = function (name, json) {
	var add = 0;
	if (json.name == name && (json.children != null || json._children != null)) {
		return 1;
	}
	if (json.children) {
		for (var i = 0, len = json.children.length; i < len; i++) {
			add += exist_in_json(name, json.children[i])
		}
	}
	if (json._children) {
		for (var i = 0, len = json._children.length; i < len; i++) {
			add += exist_in_json(name, json._children[i])
		}
	}
	return add;
}

var add_in_json = function(name, json, p) {
	if (json.name == name) {
		$.ajax({
			url: '/get_json?aide=' + name,
			error: function(a, b, c) {console.log(a, b, c);},
			success: function(data) {
				data = JSON.parse(data);
				if (data.children && json.children == null && data.children.length > 0) {
					json.children = [];
					for (var i = 0, len = data.children.length; i < len; i++) {
						json.children.push(data.children[i]);	
					}
				}
			}
		})
		return 1;
	}
	if (json.children) {
		for (var i = 0, len = json.children.length; i < len; i++) {
			if (add_in_json(name, json.children[i], p) == 1) {
				return 1;
			}
		}
	}
	return 0;
}

var get_root = function(d) {
	if (!d.parent.parent) {
		return d; 
	}
	else {
		return get_root(d.parent);
	}
}

var select_by_name = function(name, node) {
	tab = [];
	if (name == node.name) {
		return [node];
	}
	else {
		if (node.children) {
			for (var i = 0, len = node.children.length; i < len; i++) {
				t = select_by_name(name, node.children[i]);
				if (t) {
					tab.concat(t);
				}
			}
		}
		if (node._children) {
			for (var i = 0, len = node._children.length; i < len; i++) {
				var t = select_by_name(name, node._children[i]);
				if (t) {
					tab.concat(t);
				}
			}
		}
	}
	return tab;
}

var get_color_by_name = function(name) {
	if (name &&
		d3.selectAll('g.node').filter(function(d){return d.name == name}) &&
		d3.selectAll('g.node').filter(function(d){return d.name == name})[0].length > 0 &&
		d3.selectAll('g.node').filter(function(d){return d.name == name}).select('circle')) {
		return d3.selectAll('g.node').filter(function(d){return d.name == name}).select('circle').style('fill');
	}
}

var node_can_be_calculated = function(d) {
	var c = d.children ? d.children : d._children;
	if (c) {
		for (var i = 0, len = c.length; i < len; i++) {
			if (c[i] && c[i].name && get_color_by_name(c[i].name) != "rgb(144, 238, 144)") {
				return 0;
			}	
		}
	}
	else {
		return 0
	}
	if (d.parent == "ROOT") {
		setTimeout(function(){build_json('Jean-Charles', get_simulation)}, 1000);
		clearInterval(interval);
	}
	return 1;
}

var node_calculate = function(d) {
	var c = d.children ? d.children : d._children;
	if (d__data__["input_value"] != "") {
		return 1;
	}
	if (c) {
		for (var i = 0, len = c.length; i < len; i++) {
			if (node_calculate(c[i]) == 0) {
				return 0;
			}	
		}
		return 1;
	}
	return 0;
}

var update_node = function(d) {
	if (node_can_be_calculated(d)) {
		d.iscalculate = 1;
		d._children = d.children;
		d.children = null;
	}
}

var auto_extand = function(d) {
	var c = d.children ? d.children : d._children;
	if (!is_in_tab(d.name) &&
		!node_is_set(d.name) && 
		!node_can_be_calculated(d) &&
		d.__data__["nb_child"] > 0) {
			add_in_json(d.name. selected.root, d.parent.name);
		}
}

var expand  = function (d){   
	var children = (d.children)?d.children:d._children;
	if (d._children) {        
		d.children = d._children;
		d._children = null;       
	}
	if(children)
		children.forEach(expand);
}

var expandAll = function(){
	expand(root); 
}

var get_json_from_nodes = function (node) {
	if (node._children == null && node.children == null) {
		return {
			"name": node.name,
			"parent": node.parent.name
		};	
	}
	else {
		node.tb = Array(0);
		if (node.children) {
			for (var i = 0; i < node.children.length; i++) {
				node.tb.push(get_json_from_nodes(node.children[i]))
			}
		}
		if (node._children) {
			for (var i = 0; i < node.children.length; i++) {
				node.tb.push(get_json_from_nodes(node._children[i]))
			}
		}
		return {
			"name": node.name,
			"parent": node.parent.name,
			"children": node.tb
		}
	}
}
