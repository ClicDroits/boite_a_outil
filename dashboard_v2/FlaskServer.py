#! /usr/bin/python
# -*- coding:utf-8 -*-

from openfisca_france import CountryTaxBenefitSystem
from openfisca_core.simulations import Simulation
from flask import Flask, url_for, request
from flask import render_template
from Database import Database 
from inspect import getmembers
from pprint import pprint
import os
import json
import datetime


app = Flask(__name__, template_folder='./template')
db = Database()

variables =  db.getAll('prestations_variables_template_child') 


def get_nb_child(name):
    row =  db.getOne('prestations_variables_template_child', where=("name=%s", [name]))
    return len(row.children.split(':'))


@app.route('/<aide>/')
def accueil(aide):
    if (aide == None or aide == ''):
        aide = 'rsa'
    return render_template('index.html', variables=variables, aide=aide)

@app.route('/get_json')
def ret_json():
    ad = request.args.get('aide')
    row =  db.getOne('prestations_variables_template_child', where=("name=%s", [ad]))
    jsoni = json.loads(row.json);
    jsoni['nb_child'] = get_nb_child(ad);
    print ad, jsoni['nb_child'];
    return json.dumps(jsoni), 200, {'Content-Type:' : 'application/json'}

@app.route('/get_entity')
def ret_entity():
    ad = request.args.get('aide')
    row =  db.getOne('prestations_variables_template_child', where=("name=%s", [ad]))
    return row.entity, 200, {'Content-Type:' : 'text/plain'}

@app.route('/run_simulation', methods=['GET', 'POST'])
def run_simulation():
    ad = request.args.get('aide')
    js = json.loads(request.form.get('js'))
    prev_mouth = datetime.date.today() + datetime.timedelta(-0*365/12)
    fr_sys = CountryTaxBenefitSystem()
    simulation = Simulation(tax_benefit_system = fr_sys, simulation_json = js)
    res = simulation.calculate(ad, prev_mouth.strftime('%Y-%m'))
    return res[0].astype('|S10'), 200, {'Content-Type:' : 'text/plain'}

if __name__ == '__main__':
    app.run(debug=True)

